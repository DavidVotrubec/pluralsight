﻿using System;

namespace PluralSight
{
    class Program
    {
        static void Main(string[] args)
        {
            var absMethod = DebugUtils.InfoOf(() => Math.Abs(default(double)));
            var nowProperty = DebugUtils.InfoOf(() => DateTime.Now);
            var guidConstr = DebugUtils.InfoOf(() => new Guid(default(byte[])));
        }
    }
}
