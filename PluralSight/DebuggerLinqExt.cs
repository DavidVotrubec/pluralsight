﻿using System;
using System.Collections.Generic;

namespace PluralSight
{
    //Taken from PluralSight course
    public static class DebuggerLinqExt
    {
        /// <summary>
        /// Writes the output of debug function to the Console
        /// </summary>
        public static IEnumerable<T> Where<T>(this IEnumerable<T> source, Func<T, string> debug)
        {
            foreach (var item in source)
            {
                Console.WriteLine(debug(item));
                yield return item;
            }
        }
    }
    
}
